package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
    
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
    @RequestMapping("/")
    public String deleteuser() {
        return "Greetings from Spring Boot deleteuser!";
    }
     @RequestMapping("/")
    public String UPDATEuser() {
        return "Greetings from Spring Boot UPDATEuser!";
    }

    @RequestMapping("/")
    public String adduser() {
        return "Greetings from Spring Boot adduser!";
    }
}
